//
//  Constants.swift
//  Pokedex
//
//  Created by Marko Bizjak on 10/11/2016.
//  Copyright © 2016 Marko Bizjak. All rights reserved.
//

import Foundation

let URL_BASE = "http://pokeapi.co"
let URL_POKEMON = "/api/v1/pokemon/"

typealias DownloadComplete = () -> ()
